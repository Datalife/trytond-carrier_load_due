# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class CarrierLoadDueTestCase(ModuleTestCase):
    """Test Carrier Load Due module"""
    module = 'carrier_load_due'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            CarrierLoadDueTestCase))
    return suite
