datalife_carrier_load_due
=========================

The carrier_load_due module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-carrier_load_due/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-carrier_load_due)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
