from trytond.pool import PoolMeta, Pool
from trytond.model import fields, ModelView
from trytond.pyson import Eval, Not, In
from trytond.transaction import Transaction
from trytond.wizard import StateView, StateTransition, Button
from trytond.exceptions import UserError
from trytond.i18n import gettext


class LoadOrder(metaclass=PoolMeta):
    __name__ = 'carrier.load.order'

    due_export_number = fields.Function(fields.Char('Export number',
        states={'invisible': Eval('state') != 'done'},
        depends=['state']), 'get_due_number', searcher='search_due_number')
    due_transient_number = fields.Function(fields.Char('Transient number',
        states={'invisible': Eval('state') != 'done'},
        depends=['state']), 'get_due_number', searcher='search_due_number')

    @classmethod
    def __setup__(cls):
        super(LoadOrder, cls).__setup__()
        cls._buttons.update({
            'modify_due': {
                'invisible': Eval('state') != 'done',
                'icon': 'tryton-ok',
                'depends': ['state']
            }
        })

    def get_due_number(self, name=None):
        if self.shipment and self.type == 'out':
            return getattr(self.shipment, name)
        return None

    @classmethod
    def search_due_number(cls, name, clause):
        return [
            ('shipment.%s' % name,) + tuple(clause[1:]) +
            ('stock.shipment.out',)
        ]

    @classmethod
    @ModelView.button_action('carrier_load.wizard_load_order_do')
    def modify_due(cls, records):
        pass


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    due_export_number = fields.Function(fields.Char('Export number'),
        'get_due_number', searcher='search_due_number')
    due_transient_number = fields.Function(fields.Char('Transient number'),
        'get_due_number', searcher='search_due_number')

    def get_due_number(self, name=None):
        if self.shipments:
            return getattr(self.shipments[0], name)
        return None

    @classmethod
    def search_due_number(cls, name, clause):
        return [
            ('lines.moves.shipment.%s' % name,) + tuple(clause[1:]) +
            ('stock.shipment.out',)
        ]


class ShipmentOut(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    due_export_number = fields.Char('Export number',
        states={'readonly': Not(In(Eval('state'), ['draft', 'waiting']))},
        depends=['state'])
    due_transient_number = fields.Char('Transient number',
        states={'readonly': Not(In(Eval('state'), ['draft', 'waiting']))},
        depends=['state'])


class DueLoadOrderData(ModelView):
    '''Due data carrier load order'''
    __name__ = 'carrier.load.order.due_data'

    sale = fields.Many2One('sale.sale', 'Sale', readonly=True)
    shipment = fields.Char('Shipment', readonly=True)
    due_export_number = fields.Char('Export number')
    due_transient_number = fields.Char('Transient number')


class DoLoadOrder(metaclass=PoolMeta):
    __name__ = 'carrier.load.order.do'

    pre_due = StateTransition()
    due = StateView('carrier.load.order.due_data',
        'carrier_load_due.load_order_due_data_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('OK', 'finish_due', 'tryton-ok', default=True)])
    finish_due = StateTransition()

    def transition_start(self):
        Modeldata = Pool().get('ir.model.data')

        due_action = Modeldata.get_id('carrier_load_due',
            'wizard_load_order_set_due')
        if Transaction().context['action_id'] == due_action:
            if self.transition_pre_due() == 'due':
                return 'due'
            return 'end'
        return super().transition_start()

    @classmethod
    def next_states(cls):
        return super().next_states() + ['pre_due', 'finish_due']

    def transition_pre_due(self):
        pool = Pool()
        Order = pool.get('carrier.load.order')

        order = Order(Transaction().context['active_id'])

        next_ = self.next_action('finish_due')
        if order.type != 'out':
            return next_

        if order.shipment.state == 'done':
            raise UserError(gettext('carrier_load_due.'
                'msg_carrier_load_order_do_shipment_done',
                order=order.rec_name))
        if order.shipment.delivery_address:
            country = order.shipment.delivery_address.country
            if not country or not country.eu_member:
                return 'due'
        return next_

    def default_due(self, fields):
        pool = Pool()
        Order = pool.get('carrier.load.order')

        order = Order(Transaction().context['active_id'])
        return {
            'sale': order.sale.id if order.sale else None,
            'shipment': order.shipment.number,
            'due_export_number': order.due_export_number,
            'due_transient_number': order.due_transient_number
        }

    def transition_finish_due(self):
        pool = Pool()
        Order = pool.get('carrier.load.order')

        order = Order(Transaction().context['active_id'])
        if order.type == 'out' and order.shipment:
            order.shipment.due_export_number = self.due.due_export_number
            order.shipment.due_transient_number = \
                self.due.due_transient_number
            order.shipment.save()
        return self.next_action('finish_due')
